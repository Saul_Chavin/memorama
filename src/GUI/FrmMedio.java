package GUI;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.Timer;

public class FrmMedio extends javax.swing.JFrame {

    Timer tInicio, tIncorrectas, tCorrectas;
    String[][] URLimagen = new String[16][2];
    ArrayList<JLabel> labels = new ArrayList<JLabel>();
    Random numAleatorio = new Random();
    String carta1 = "";
    String carta2 = "";
    int[] nums = new int[16];
    int indice1, indice2;
    int seg = 0;//segundos para manipular el Timer
    int errores = 0;
    int win = 0;
    int cont = 0;

    public FrmMedio() {
        initComponents();
        setSize(1200, 670);
        setLocationRelativeTo(null);
        Lwin.setText("Presiona Backspace si quieres salir");
        agregarEtiquetas();
        cargarImagenAetiqueta();
        TiempoAlIniciar();
        tInicio.start();

    }

    public void agregarEtiquetas() {
        labels.add(Lcard1);
        labels.add(Lcard9);
        labels.add(Lcard2);
        labels.add(Lcard10);
        labels.add(Lcard3);
        labels.add(Lcard11);
        labels.add(Lcard4);
        labels.add(Lcard12);
        labels.add(Lcard5);
        labels.add(Lcard13);
        labels.add(Lcard6);
        labels.add(Lcard14);
        labels.add(Lcard7);
        labels.add(Lcard15);
        labels.add(Lcard8);
        labels.add(Lcard16);
        labels.add(Lcard17);
        labels.add(Lcard18);
        labels.add(Lcard19);
        labels.add(Lcard20);
        labels.add(Lcard21);
        labels.add(Lcard22);
        labels.add(Lcard23);
        labels.add(Lcard24);
        labels.add(Lcard25);
        labels.add(Lcard26);
        labels.add(Lcard27);
        labels.add(Lcard28);
        labels.add(Lcard29);
        labels.add(Lcard30);
        labels.add(Lcard31);
        labels.add(Lcard32);
    }

    public void cargarImagenAetiqueta() {
        for (int i = 0; i < nums.length; i++) {
            nums[i] = i + 1;
        }
        for (int i = nums.length; i > 0; i--) {
            int numeros = numAleatorio.nextInt(i);
            int tmp = nums[i - 1];
            nums[i - 1] = nums[numeros];
            nums[numeros] = tmp;
            URLimagen[i - 1][0] = "/cartas/carta " + nums[i - 1] + ".png";
            System.out.println(URLimagen[i - 1][0]);
        }

        for (int i = nums.length; i > 0; i--) {
            int numeros = numAleatorio.nextInt(i);
            int tmp = nums[i - 1];
            nums[i - 1] = nums[numeros];
            nums[numeros] = tmp;
            URLimagen[i - 1][1] = "/cartas/carta " + nums[i - 1] + ".png";
            System.out.println(URLimagen[i - 1][1]);
        }

        for (int i = 0; i < URLimagen.length; i++) {

            labels.get(i).setIcon(new ImageIcon(getClass().getResource(URLimagen[i][0])));
            labels.get(i + 16).setIcon(new ImageIcon(getClass().getResource(URLimagen[i][1])));

        }
    }

    public void TiempoAlIniciar() {
        tInicio = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                seg++;

                if (tInicio.isRunning() && seg == 3) {
                    seg = 0;
                    tInicio.stop();
                    Lwin.setText("");
                    ocultarTodasLasCartas();
                }
            }
        });
    }

    public void TiempoIncorrectas() {
        tIncorrectas = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                seg++;

                if (tIncorrectas.isRunning() && seg == 1) {
                    seg = 0;
                    tIncorrectas.stop();

                    labels.get(indice1).setIcon(new ImageIcon(getClass().getResource("/img/Tapada.png")));
                    labels.get(indice2).setIcon(new ImageIcon(getClass().getResource("/img/Tapada.png")));
                    cont=0;
                    Lacierto_error.setIcon(null);

                }
            }
        });
    }

    public void TiempoCorrectas() {
        tCorrectas = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                seg++;

                if (tCorrectas.isRunning() && seg == 1) {
                    seg = 0;
                    tCorrectas.stop();
                    labels.get(indice1).setVisible(false);
                    labels.get(indice2).setVisible(false);
                    cont=0;
                    Lacierto_error.setIcon(null);

                }
            }
        });
    }

    public void ocultarTodasLasCartas() {
        for (int i = 0; i < URLimagen.length; i++) {
            labels.get(i).setIcon(new ImageIcon(getClass().getResource("/img/Tapada.png")));
            labels.get(i + 16).setIcon(new ImageIcon(getClass().getResource("/img/Tapada.png")));

        }
    }

    public void destaparCarta(JLabel l) {
        int indiceEtiqueta = labels.indexOf(l);//tomamos el indice de la etiqueta que mandaremos como parametro
        String carta;
       
        if(cont < 2){
             if (indiceEtiqueta < 16) {
            carta = URLimagen[indiceEtiqueta][0];
        } else {
            carta = URLimagen[indiceEtiqueta - 16][1];
        }
        l.setIcon(new ImageIcon(getClass().getResource(carta)));

        if (carta1.equals("")) {
            carta1 = carta;
            indice1 = indiceEtiqueta;
        } else {
            carta2 = carta;
            indice2 = indiceEtiqueta;
        }
        }
        if(indice1==indice2){
            indice2=0;
        }else{
            cont++;
            if (cont == 2) {
            if (carta1.equals(carta2)) {
                win++;
                Lacierto_error.setIcon(new ImageIcon(getClass().getResource("img/correct.png")));
                TiempoCorrectas();
                tCorrectas.start();
            } else {
                errores++;
                 Lacierto_error.setIcon(new ImageIcon(getClass().getResource("img/error.png")));
                Lintentos.setText("" + errores);
                TiempoIncorrectas();
                tIncorrectas.start();
            }
            carta1 = "";
            carta2 = "";
        }
        if (win == 16) {
            Lwin.setText("Has Terminado, presiona Enter para salir...");
//            Lintentos.setLocation((getWidth()/2-300), (getHeight()/2));
//            Lintentos.setText("intentos fallidos= " + errores);
        }
        }
       
        

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Lacierto_error = new javax.swing.JLabel();
        Lwin = new javax.swing.JLabel();
        Lintentos = new javax.swing.JLabel();
        Lcard1 = new javax.swing.JLabel();
        Lcard2 = new javax.swing.JLabel();
        Lcard3 = new javax.swing.JLabel();
        Lcard4 = new javax.swing.JLabel();
        Lcard5 = new javax.swing.JLabel();
        Lcard6 = new javax.swing.JLabel();
        Lcard7 = new javax.swing.JLabel();
        Lcard8 = new javax.swing.JLabel();
        Lcard9 = new javax.swing.JLabel();
        Lcard10 = new javax.swing.JLabel();
        Lcard11 = new javax.swing.JLabel();
        Lcard12 = new javax.swing.JLabel();
        Lcard13 = new javax.swing.JLabel();
        Lcard14 = new javax.swing.JLabel();
        Lcard15 = new javax.swing.JLabel();
        Lcard16 = new javax.swing.JLabel();
        Lcard17 = new javax.swing.JLabel();
        Lcard18 = new javax.swing.JLabel();
        Lcard19 = new javax.swing.JLabel();
        Lcard20 = new javax.swing.JLabel();
        Lcard21 = new javax.swing.JLabel();
        Lcard22 = new javax.swing.JLabel();
        Lcard23 = new javax.swing.JLabel();
        Lcard24 = new javax.swing.JLabel();
        Lcard25 = new javax.swing.JLabel();
        Lcard26 = new javax.swing.JLabel();
        Lcard27 = new javax.swing.JLabel();
        Lcard28 = new javax.swing.JLabel();
        Lcard29 = new javax.swing.JLabel();
        Lcard30 = new javax.swing.JLabel();
        Lcard31 = new javax.swing.JLabel();
        Lcard32 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });
        getContentPane().setLayout(null);

        Lacierto_error.setFont(new java.awt.Font("04b", 0, 18)); // NOI18N
        Lacierto_error.setForeground(new java.awt.Color(0, 0, 0));
        getContentPane().add(Lacierto_error);
        Lacierto_error.setBounds(960, 50, 240, 120);

        Lwin.setFont(new java.awt.Font("04b", 0, 24)); // NOI18N
        Lwin.setForeground(new java.awt.Color(0, 0, 0));
        Lwin.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        getContentPane().add(Lwin);
        Lwin.setBounds(70, 290, 1060, 60);

        Lintentos.setFont(new java.awt.Font("04b", 0, 36)); // NOI18N
        Lintentos.setForeground(new java.awt.Color(0, 0, 0));
        getContentPane().add(Lintentos);
        Lintentos.setBounds(350, 0, 240, 50);

        Lcard1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard1MousePressed(evt);
            }
        });
        getContentPane().add(Lcard1);
        Lcard1.setBounds(220, 50, 100, 138);

        Lcard2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard2MousePressed(evt);
            }
        });
        getContentPane().add(Lcard2);
        Lcard2.setBounds(100, 50, 100, 138);

        Lcard3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard3MousePressed(evt);
            }
        });
        getContentPane().add(Lcard3);
        Lcard3.setBounds(460, 50, 100, 138);

        Lcard4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard4MousePressed(evt);
            }
        });
        getContentPane().add(Lcard4);
        Lcard4.setBounds(700, 50, 100, 138);

        Lcard5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard5MousePressed(evt);
            }
        });
        getContentPane().add(Lcard5);
        Lcard5.setBounds(220, 190, 100, 138);

        Lcard6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard6MousePressed(evt);
            }
        });
        getContentPane().add(Lcard6);
        Lcard6.setBounds(100, 330, 100, 138);

        Lcard7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard7MousePressed(evt);
            }
        });
        getContentPane().add(Lcard7);
        Lcard7.setBounds(940, 50, 100, 138);

        Lcard8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard8MousePressed(evt);
            }
        });
        getContentPane().add(Lcard8);
        Lcard8.setBounds(820, 190, 100, 138);

        Lcard9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard9MousePressed(evt);
            }
        });
        getContentPane().add(Lcard9);
        Lcard9.setBounds(340, 50, 100, 138);

        Lcard10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard10MousePressed(evt);
            }
        });
        getContentPane().add(Lcard10);
        Lcard10.setBounds(100, 190, 100, 138);

        Lcard11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard11MousePressed(evt);
            }
        });
        getContentPane().add(Lcard11);
        Lcard11.setBounds(340, 190, 100, 138);

        Lcard12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard12MousePressed(evt);
            }
        });
        getContentPane().add(Lcard12);
        Lcard12.setBounds(820, 50, 100, 138);

        Lcard13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard13MousePressed(evt);
            }
        });
        getContentPane().add(Lcard13);
        Lcard13.setBounds(460, 190, 100, 138);

        Lcard14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard14.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard14MousePressed(evt);
            }
        });
        getContentPane().add(Lcard14);
        Lcard14.setBounds(580, 190, 100, 138);

        Lcard15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard15.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard15MousePressed(evt);
            }
        });
        getContentPane().add(Lcard15);
        Lcard15.setBounds(700, 190, 100, 138);

        Lcard16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard16.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard16MousePressed(evt);
            }
        });
        getContentPane().add(Lcard16);
        Lcard16.setBounds(940, 190, 100, 138);

        Lcard17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard17.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard17MousePressed(evt);
            }
        });
        getContentPane().add(Lcard17);
        Lcard17.setBounds(580, 50, 100, 138);

        Lcard18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard18MousePressed(evt);
            }
        });
        getContentPane().add(Lcard18);
        Lcard18.setBounds(100, 470, 100, 138);

        Lcard19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard19MousePressed(evt);
            }
        });
        getContentPane().add(Lcard19);
        Lcard19.setBounds(340, 330, 100, 138);

        Lcard20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard20MousePressed(evt);
            }
        });
        getContentPane().add(Lcard20);
        Lcard20.setBounds(580, 330, 100, 138);

        Lcard21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard21MousePressed(evt);
            }
        });
        getContentPane().add(Lcard21);
        Lcard21.setBounds(220, 470, 100, 138);

        Lcard22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard22.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard22MousePressed(evt);
            }
        });
        getContentPane().add(Lcard22);
        Lcard22.setBounds(700, 330, 100, 138);

        Lcard23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard23.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard23MousePressed(evt);
            }
        });
        getContentPane().add(Lcard23);
        Lcard23.setBounds(580, 470, 100, 138);

        Lcard24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard24.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard24MousePressed(evt);
            }
        });
        getContentPane().add(Lcard24);
        Lcard24.setBounds(940, 330, 100, 138);

        Lcard25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard25.setText("jLabel2");
        Lcard25.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard25MousePressed(evt);
            }
        });
        getContentPane().add(Lcard25);
        Lcard25.setBounds(940, 470, 100, 138);

        Lcard26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard26.setText("jLabel2");
        Lcard26.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard26MousePressed(evt);
            }
        });
        getContentPane().add(Lcard26);
        Lcard26.setBounds(220, 330, 100, 138);

        Lcard27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard27.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard27MousePressed(evt);
            }
        });
        getContentPane().add(Lcard27);
        Lcard27.setBounds(460, 330, 100, 138);

        Lcard28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard28.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard28MousePressed(evt);
            }
        });
        getContentPane().add(Lcard28);
        Lcard28.setBounds(340, 470, 100, 138);

        Lcard29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard29.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard29MousePressed(evt);
            }
        });
        getContentPane().add(Lcard29);
        Lcard29.setBounds(460, 470, 100, 138);

        Lcard30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard30.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard30MousePressed(evt);
            }
        });
        getContentPane().add(Lcard30);
        Lcard30.setBounds(700, 470, 100, 138);

        Lcard31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard31.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard31MousePressed(evt);
            }
        });
        getContentPane().add(Lcard31);
        Lcard31.setBounds(820, 330, 100, 138);

        Lcard32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard32.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard32MousePressed(evt);
            }
        });
        getContentPane().add(Lcard32);
        Lcard32.setBounds(820, 470, 100, 138);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/FondoNivel1.jpg"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 1200, 630);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Lcard2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard2MousePressed
        destaparCarta(Lcard2);
    }//GEN-LAST:event_Lcard2MousePressed

    private void Lcard1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard1MousePressed
        destaparCarta(Lcard1);
    }//GEN-LAST:event_Lcard1MousePressed

    private void Lcard9MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard9MousePressed
        destaparCarta(Lcard9);
    }//GEN-LAST:event_Lcard9MousePressed

    private void Lcard3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard3MousePressed
        destaparCarta(Lcard3);
    }//GEN-LAST:event_Lcard3MousePressed

    private void Lcard17MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard17MousePressed
        destaparCarta(Lcard17);
    }//GEN-LAST:event_Lcard17MousePressed

    private void Lcard4MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard4MousePressed
        destaparCarta(Lcard4);
    }//GEN-LAST:event_Lcard4MousePressed

    private void Lcard12MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard12MousePressed
        destaparCarta(Lcard12);
    }//GEN-LAST:event_Lcard12MousePressed

    private void Lcard7MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard7MousePressed
        destaparCarta(Lcard7);
    }//GEN-LAST:event_Lcard7MousePressed

    private void Lcard10MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard10MousePressed
        destaparCarta(Lcard10);
    }//GEN-LAST:event_Lcard10MousePressed

    private void Lcard5MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard5MousePressed
        destaparCarta(Lcard5);
    }//GEN-LAST:event_Lcard5MousePressed

    private void Lcard11MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard11MousePressed
        destaparCarta(Lcard11);
    }//GEN-LAST:event_Lcard11MousePressed

    private void Lcard13MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard13MousePressed
        destaparCarta(Lcard13);
    }//GEN-LAST:event_Lcard13MousePressed

    private void Lcard14MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard14MousePressed
        destaparCarta(Lcard14);
    }//GEN-LAST:event_Lcard14MousePressed

    private void Lcard15MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard15MousePressed
        destaparCarta(Lcard15);
    }//GEN-LAST:event_Lcard15MousePressed

    private void Lcard8MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard8MousePressed
        destaparCarta(Lcard8);
    }//GEN-LAST:event_Lcard8MousePressed

    private void Lcard16MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard16MousePressed
        destaparCarta(Lcard16);
    }//GEN-LAST:event_Lcard16MousePressed

    private void Lcard6MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard6MousePressed
        destaparCarta(Lcard6);
    }//GEN-LAST:event_Lcard6MousePressed

    private void Lcard26MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard26MousePressed
        destaparCarta(Lcard26);
    }//GEN-LAST:event_Lcard26MousePressed

    private void Lcard19MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard19MousePressed
        destaparCarta(Lcard19);
    }//GEN-LAST:event_Lcard19MousePressed

    private void Lcard27MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard27MousePressed
        destaparCarta(Lcard27);
    }//GEN-LAST:event_Lcard27MousePressed

    private void Lcard20MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard20MousePressed
        destaparCarta(Lcard20);
    }//GEN-LAST:event_Lcard20MousePressed

    private void Lcard22MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard22MousePressed
        destaparCarta(Lcard22);
    }//GEN-LAST:event_Lcard22MousePressed

    private void Lcard31MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard31MousePressed
        destaparCarta(Lcard31);
    }//GEN-LAST:event_Lcard31MousePressed

    private void Lcard24MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard24MousePressed
        destaparCarta(Lcard24);

    }//GEN-LAST:event_Lcard24MousePressed

    private void Lcard18MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard18MousePressed
        destaparCarta(Lcard18);

    }//GEN-LAST:event_Lcard18MousePressed

    private void Lcard21MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard21MousePressed
        destaparCarta(Lcard21);

    }//GEN-LAST:event_Lcard21MousePressed

    private void Lcard28MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard28MousePressed
        destaparCarta(Lcard28);

    }//GEN-LAST:event_Lcard28MousePressed

    private void Lcard29MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard29MousePressed
        destaparCarta(Lcard29);

    }//GEN-LAST:event_Lcard29MousePressed

    private void Lcard23MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard23MousePressed
        destaparCarta(Lcard23);

    }//GEN-LAST:event_Lcard23MousePressed

    private void Lcard30MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard30MousePressed
        destaparCarta(Lcard30);

    }//GEN-LAST:event_Lcard30MousePressed

    private void Lcard32MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard32MousePressed
        destaparCarta(Lcard32);

    }//GEN-LAST:event_Lcard32MousePressed

    private void Lcard25MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard25MousePressed
        destaparCarta(Lcard25);

    }//GEN-LAST:event_Lcard25MousePressed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            if(win>=16){
                this.dispose();
                FrmInicio fi = new FrmInicio();
                fi.setVisible(true);
            }
        }
        if(evt.getKeyCode() == KeyEvent.VK_BACK_SPACE){
             this.dispose();
                FrmInicio fi = new FrmInicio();
                fi.setVisible(true);
        }
    }//GEN-LAST:event_formKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmMedio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmMedio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmMedio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmMedio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmMedio().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Lacierto_error;
    private javax.swing.JLabel Lcard1;
    private javax.swing.JLabel Lcard10;
    private javax.swing.JLabel Lcard11;
    private javax.swing.JLabel Lcard12;
    private javax.swing.JLabel Lcard13;
    private javax.swing.JLabel Lcard14;
    private javax.swing.JLabel Lcard15;
    private javax.swing.JLabel Lcard16;
    private javax.swing.JLabel Lcard17;
    private javax.swing.JLabel Lcard18;
    private javax.swing.JLabel Lcard19;
    private javax.swing.JLabel Lcard2;
    private javax.swing.JLabel Lcard20;
    private javax.swing.JLabel Lcard21;
    private javax.swing.JLabel Lcard22;
    private javax.swing.JLabel Lcard23;
    private javax.swing.JLabel Lcard24;
    private javax.swing.JLabel Lcard25;
    private javax.swing.JLabel Lcard26;
    private javax.swing.JLabel Lcard27;
    private javax.swing.JLabel Lcard28;
    private javax.swing.JLabel Lcard29;
    private javax.swing.JLabel Lcard3;
    private javax.swing.JLabel Lcard30;
    private javax.swing.JLabel Lcard31;
    private javax.swing.JLabel Lcard32;
    private javax.swing.JLabel Lcard4;
    private javax.swing.JLabel Lcard5;
    private javax.swing.JLabel Lcard6;
    private javax.swing.JLabel Lcard7;
    private javax.swing.JLabel Lcard8;
    private javax.swing.JLabel Lcard9;
    private javax.swing.JLabel Lintentos;
    private javax.swing.JLabel Lwin;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
