package GUI;


import javax.swing.ImageIcon;





public class FrmInicio extends javax.swing.JFrame {
    
    public FrmInicio() {
        initComponents();
       this.setSize(1200,650);
       this.setLocationRelativeTo(null);
       Lfondo.setSize(1200,630);
       
      
       BtnFacil.setLocation((BtnFacil.getX()*8), BtnFacil.getY());
       BtnMedio.setLocation((BtnMedio.getX()*8), BtnMedio.getY());
       BtnDificil.setLocation((BtnDificil.getX()*8),BtnDificil.getY());
       Ltext.setLocation(Ltext.getX()+100, Ltext.getY());
       
       
    }
    
    
    

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BtnFacil = new javax.swing.JButton();
        BtnMedio = new javax.swing.JButton();
        BtnDificil = new javax.swing.JButton();
        Ltext = new javax.swing.JLabel();
        Lfondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        BtnFacil.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/facilNoselect.png"))); // NOI18N
        BtnFacil.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        BtnFacil.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Facil.png"))); // NOI18N
        BtnFacil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnFacilActionPerformed(evt);
            }
        });
        getContentPane().add(BtnFacil);
        BtnFacil.setBounds(110, 210, 200, 60);

        BtnMedio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/medioNoselect.png"))); // NOI18N
        BtnMedio.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Medio.png"))); // NOI18N
        BtnMedio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnMedioActionPerformed(evt);
            }
        });
        getContentPane().add(BtnMedio);
        BtnMedio.setBounds(110, 300, 200, 60);

        BtnDificil.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/dificilNoselect.png"))); // NOI18N
        BtnDificil.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/img/dificil.png"))); // NOI18N
        BtnDificil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDificilActionPerformed(evt);
            }
        });
        getContentPane().add(BtnDificil);
        BtnDificil.setBounds(110, 390, 200, 60);

        Ltext.setFont(new java.awt.Font("NSimSun", 3, 24)); // NOI18N
        Ltext.setForeground(new java.awt.Color(102, 0, 0));
        Ltext.setText("Elige el nivel de dificultad:");
        getContentPane().add(Ltext);
        Ltext.setBounds(590, 130, 390, 29);

        Lfondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/FondoInicioMemorama.jpg"))); // NOI18N
        getContentPane().add(Lfondo);
        Lfondo.setBounds(0, 0, 810, 470);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnFacilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnFacilActionPerformed
       FrmFacil ff = new FrmFacil();
       ff.setVisible(true);
       this.dispose();
    }//GEN-LAST:event_BtnFacilActionPerformed

    private void BtnMedioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnMedioActionPerformed
      FrmMedio fm = new FrmMedio();
      fm.setVisible(true);
      this.dispose();
    }//GEN-LAST:event_BtnMedioActionPerformed

    private void BtnDificilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDificilActionPerformed
        FrmDificil fd = new FrmDificil();
        fd.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnDificilActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmInicio().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnDificil;
    private javax.swing.JButton BtnFacil;
    private javax.swing.JButton BtnMedio;
    private javax.swing.JLabel Lfondo;
    private javax.swing.JLabel Ltext;
    // End of variables declaration//GEN-END:variables
}
