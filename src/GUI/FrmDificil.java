package GUI;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.Timer;


public class FrmDificil extends javax.swing.JFrame {

    Timer tInicio, tIncorrectas, tCorrectas;
    String[][] URLimagen = new String[32][2];
    ArrayList<JLabel> labels = new ArrayList<JLabel>();
    Random numAleatorio = new Random();
    String carta1 = "";
    String carta2 = "";
    int[] nums = new int[32];
    int indice1, indice2;
    int seg = 0;//segundos para manipular el Timer
    int errores = 0;
    int win = 0;
    int cont = 0;
    public FrmDificil() {
        initComponents();
        setSize(1640, 860);
        Lwin.setText("Presiona Backspace si quieres salir");
        agregarEtiquetas();
        cargarImagenAetiqueta();
        TiempoAlIniciar();
        tInicio.start();
    }
    
     public void agregarEtiquetas() {
        labels.add(Lcard1);
        labels.add(Lcard9);
        labels.add(Lcard2);
        labels.add(Lcard10);
        labels.add(Lcard3);
        labels.add(Lcard11);
        labels.add(Lcard4);
        labels.add(Lcard12);
        labels.add(Lcard5);
        labels.add(Lcard13);
        labels.add(Lcard6);
        labels.add(Lcard14);
        labels.add(Lcard7);
        labels.add(Lcard15);
        labels.add(Lcard8);
        labels.add(Lcard16);
        labels.add(Lcard17);
        labels.add(Lcard18);
        labels.add(Lcard19);
        labels.add(Lcard20);
        labels.add(Lcard21);
        labels.add(Lcard22);
        labels.add(Lcard23);
        labels.add(Lcard24);
        labels.add(Lcard25);
        labels.add(Lcard26);
        labels.add(Lcard27);
        labels.add(Lcard28);
        labels.add(Lcard29);
        labels.add(Lcard30);
        labels.add(Lcard31);
        labels.add(Lcard32);
        labels.add(Lcard33);
        labels.add(Lcard34);
        labels.add(Lcard35);
        labels.add(Lcard36);
        labels.add(Lcard37);
        labels.add(Lcard38);
        labels.add(Lcard39);
        labels.add(Lcard40);
        labels.add(Lcard41);
        labels.add(Lcard42);
        labels.add(Lcard43);
        labels.add(Lcard44);
        labels.add(Lcard45);
        labels.add(Lcard46);
        labels.add(Lcard47);
        labels.add(Lcard48);
        labels.add(Lcard49);
        labels.add(Lcard50);
        labels.add(Lcard51);
        labels.add(Lcard52);
        labels.add(Lcard53);
        labels.add(Lcard54);
        labels.add(Lcard55);
        labels.add(Lcard56);
        labels.add(Lcard57);
        labels.add(Lcard58);
        labels.add(Lcard59);
        labels.add(Lcard60);
        labels.add(Lcard61);
        labels.add(Lcard62);
        labels.add(Lcard63);
        labels.add(Lcard64);
    }

     public void cargarImagenAetiqueta() {
        for (int i = 0; i < nums.length; i++) {
            nums[i] = i + 1;
        }
        for (int i = nums.length; i > 0; i--) {
            int numeros = numAleatorio.nextInt(i);
            int tmp = nums[i - 1];
            nums[i - 1] = nums[numeros];
            nums[numeros] = tmp;
            URLimagen[i - 1][0] = "/cartasDificil/carta " + nums[i - 1] + ".png";
            System.out.println(URLimagen[i - 1][0]);
        }

        for (int i = nums.length; i > 0; i--) {
            int numeros = numAleatorio.nextInt(i);
            int tmp = nums[i - 1];
            nums[i - 1] = nums[numeros];
            nums[numeros] = tmp;
            URLimagen[i - 1][1] = "/cartasDificil/carta " + nums[i - 1] + ".png";
            System.out.println(URLimagen[i - 1][1]);
        }

        for (int i = 0; i < URLimagen.length; i++) {

            labels.get(i).setIcon(new ImageIcon(getClass().getResource(URLimagen[i][0])));
            labels.get(i + 32).setIcon(new ImageIcon(getClass().getResource(URLimagen[i][1])));

        }
    }
     
      public void TiempoAlIniciar() {
        tInicio = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                seg++;

                if (tInicio.isRunning() && seg == 3) {
                    seg = 0;
                    tInicio.stop();
                    Lwin.setText("");
                    ocultarTodasLasCartas();
                }
            }
        });
    }
      
      public void ocultarTodasLasCartas() {
        for (int i = 0; i < URLimagen.length; i++) {
            labels.get(i).setIcon(new ImageIcon(getClass().getResource("/cartasDificil/Tapada.png")));
            labels.get(i + 32).setIcon(new ImageIcon(getClass().getResource("/cartasDificil/Tapada.png")));

        }
    }
      
       public void TiempoIncorrectas() {
        tIncorrectas = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                seg++;

                if (tIncorrectas.isRunning() && seg == 1) {
                    seg = 0;
                    tIncorrectas.stop();
                    labels.get(indice1).setIcon(new ImageIcon(getClass().getResource("/cartasDificil/Tapada.png")));
                    labels.get(indice2).setIcon(new ImageIcon(getClass().getResource("/cartasDificil/Tapada.png")));
                    cont=0;
                     Lacierto_error.setIcon(null);

                }
            }
        });
    }

    public void TiempoCorrectas() {
        tCorrectas = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                seg++;

                if (tCorrectas.isRunning() && seg == 1) {
                    seg = 0;
                    tCorrectas.stop();
                    labels.get(indice1).setVisible(false);
                    labels.get(indice2).setVisible(false);
                    cont=0;
                   Lacierto_error.setIcon(null);

                }
            }
        });
    }
    
     public void destaparCarta(JLabel l) {
        int indiceEtiqueta = labels.indexOf(l);//tomamos el indice de la etiqueta que mandaremos como parametro
        String carta;
        
        if(cont<2){
            if (indiceEtiqueta < 32) {
            carta = URLimagen[indiceEtiqueta][0];
        } else {
            carta = URLimagen[indiceEtiqueta - 32][1];
        }
        l.setIcon(new ImageIcon(getClass().getResource(carta)));

        if (carta1.equals("")) {
            carta1 = carta;
            indice1 = indiceEtiqueta;
        } else {
            carta2 = carta;
            indice2 = indiceEtiqueta;
        }
        }
        if(indice1==indice2){
            indice2=0;
        }else{
            cont++;
             if (cont == 2) {
            if (carta1.equals(carta2)) {
                win++;
                Lacierto_error.setIcon(new ImageIcon(getClass().getResource("img/correct.png")));
                TiempoCorrectas();
                tCorrectas.start();
            } else {
                errores++;
                Lacierto_error.setIcon(new ImageIcon(getClass().getResource("img/error.png")));
                Lintentos.setText("" + errores);
                TiempoIncorrectas();
                tIncorrectas.start();
            }
            
            carta1 = "";
            carta2 = "";
        }
        if (win == 32) {
            Lwin.setText("Has Terminado, presiona Enter para salir...");
        }
        }
        
       

    }
  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Lacierto_error = new javax.swing.JLabel();
        Lcard1 = new javax.swing.JLabel();
        Lcard2 = new javax.swing.JLabel();
        Lcard3 = new javax.swing.JLabel();
        Lcard4 = new javax.swing.JLabel();
        Lcard5 = new javax.swing.JLabel();
        Lcard6 = new javax.swing.JLabel();
        Lcard7 = new javax.swing.JLabel();
        Lcard8 = new javax.swing.JLabel();
        Lcard9 = new javax.swing.JLabel();
        Lcard10 = new javax.swing.JLabel();
        Lcard11 = new javax.swing.JLabel();
        Lcard12 = new javax.swing.JLabel();
        Lcard13 = new javax.swing.JLabel();
        Lcard14 = new javax.swing.JLabel();
        Lcard15 = new javax.swing.JLabel();
        Lcard16 = new javax.swing.JLabel();
        Lcard17 = new javax.swing.JLabel();
        Lcard18 = new javax.swing.JLabel();
        Lcard19 = new javax.swing.JLabel();
        Lcard20 = new javax.swing.JLabel();
        Lcard21 = new javax.swing.JLabel();
        Lcard22 = new javax.swing.JLabel();
        Lcard23 = new javax.swing.JLabel();
        Lcard24 = new javax.swing.JLabel();
        Lcard25 = new javax.swing.JLabel();
        Lcard26 = new javax.swing.JLabel();
        Lcard27 = new javax.swing.JLabel();
        Lcard28 = new javax.swing.JLabel();
        Lcard29 = new javax.swing.JLabel();
        Lcard30 = new javax.swing.JLabel();
        Lcard31 = new javax.swing.JLabel();
        Lcard32 = new javax.swing.JLabel();
        Lcard33 = new javax.swing.JLabel();
        Lcard34 = new javax.swing.JLabel();
        Lcard35 = new javax.swing.JLabel();
        Lcard36 = new javax.swing.JLabel();
        Lcard37 = new javax.swing.JLabel();
        Lcard38 = new javax.swing.JLabel();
        Lcard39 = new javax.swing.JLabel();
        Lcard40 = new javax.swing.JLabel();
        Lcard41 = new javax.swing.JLabel();
        Lcard42 = new javax.swing.JLabel();
        Lcard43 = new javax.swing.JLabel();
        Lcard44 = new javax.swing.JLabel();
        Lcard45 = new javax.swing.JLabel();
        Lcard46 = new javax.swing.JLabel();
        Lcard47 = new javax.swing.JLabel();
        Lcard48 = new javax.swing.JLabel();
        Lcard49 = new javax.swing.JLabel();
        Lcard50 = new javax.swing.JLabel();
        Lcard51 = new javax.swing.JLabel();
        Lcard52 = new javax.swing.JLabel();
        Lcard53 = new javax.swing.JLabel();
        Lcard54 = new javax.swing.JLabel();
        Lcard55 = new javax.swing.JLabel();
        Lcard56 = new javax.swing.JLabel();
        Lcard57 = new javax.swing.JLabel();
        Lcard58 = new javax.swing.JLabel();
        Lcard59 = new javax.swing.JLabel();
        Lcard60 = new javax.swing.JLabel();
        Lcard61 = new javax.swing.JLabel();
        Lcard62 = new javax.swing.JLabel();
        Lcard63 = new javax.swing.JLabel();
        Lcard64 = new javax.swing.JLabel();
        Lwin = new javax.swing.JLabel();
        Lintentos = new javax.swing.JLabel();
        Lfondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });
        getContentPane().setLayout(null);

        Lacierto_error.setFont(new java.awt.Font("04b", 0, 24)); // NOI18N
        Lacierto_error.setForeground(new java.awt.Color(0, 0, 0));
        Lacierto_error.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        getContentPane().add(Lacierto_error);
        Lacierto_error.setBounds(1150, 10, 250, 110);

        Lcard1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Tapada.png"))); // NOI18N
        Lcard1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard1MousePressed(evt);
            }
        });
        getContentPane().add(Lcard1);
        Lcard1.setBounds(660, 410, 70, 138);

        Lcard2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard2MousePressed(evt);
            }
        });
        getContentPane().add(Lcard2);
        Lcard2.setBounds(580, 410, 70, 0);

        Lcard3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard3MousePressed(evt);
            }
        });
        getContentPane().add(Lcard3);
        Lcard3.setBounds(260, 410, 70, 0);

        Lcard4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard4MousePressed(evt);
            }
        });
        getContentPane().add(Lcard4);
        Lcard4.setBounds(340, 410, 70, 0);

        Lcard5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard5MousePressed(evt);
            }
        });
        getContentPane().add(Lcard5);
        Lcard5.setBounds(180, 410, 70, 0);

        Lcard6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard6MousePressed(evt);
            }
        });
        getContentPane().add(Lcard6);
        Lcard6.setBounds(420, 410, 70, 0);

        Lcard7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard7MousePressed(evt);
            }
        });
        getContentPane().add(Lcard7);
        Lcard7.setBounds(500, 410, 70, 0);

        Lcard8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard8MousePressed(evt);
            }
        });
        getContentPane().add(Lcard8);
        Lcard8.setBounds(740, 410, 70, 0);

        Lcard9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard9MousePressed(evt);
            }
        });
        getContentPane().add(Lcard9);
        Lcard9.setBounds(820, 410, 70, 0);

        Lcard10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard10MousePressed(evt);
            }
        });
        getContentPane().add(Lcard10);
        Lcard10.setBounds(900, 410, 70, 0);

        Lcard11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard11MousePressed(evt);
            }
        });
        getContentPane().add(Lcard11);
        Lcard11.setBounds(980, 410, 70, 0);

        Lcard12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard12MousePressed(evt);
            }
        });
        getContentPane().add(Lcard12);
        Lcard12.setBounds(1060, 410, 70, 0);

        Lcard13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard13MousePressed(evt);
            }
        });
        getContentPane().add(Lcard13);
        Lcard13.setBounds(1140, 410, 70, 0);

        Lcard14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard14.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard14MousePressed(evt);
            }
        });
        getContentPane().add(Lcard14);
        Lcard14.setBounds(100, 410, 70, 0);

        Lcard15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard15.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard15MousePressed(evt);
            }
        });
        getContentPane().add(Lcard15);
        Lcard15.setBounds(180, 300, 70, 0);

        Lcard16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard16.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard16MousePressed(evt);
            }
        });
        getContentPane().add(Lcard16);
        Lcard16.setBounds(260, 300, 70, 0);

        Lcard17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard17.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard17MousePressed(evt);
            }
        });
        getContentPane().add(Lcard17);
        Lcard17.setBounds(340, 300, 70, 0);

        Lcard18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard18MousePressed(evt);
            }
        });
        getContentPane().add(Lcard18);
        Lcard18.setBounds(420, 300, 70, 0);

        Lcard19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard19MousePressed(evt);
            }
        });
        getContentPane().add(Lcard19);
        Lcard19.setBounds(500, 300, 70, 0);

        Lcard20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard20MousePressed(evt);
            }
        });
        getContentPane().add(Lcard20);
        Lcard20.setBounds(580, 300, 70, 0);

        Lcard21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard21MousePressed(evt);
            }
        });
        getContentPane().add(Lcard21);
        Lcard21.setBounds(660, 300, 70, 0);

        Lcard22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard22.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard22MousePressed(evt);
            }
        });
        getContentPane().add(Lcard22);
        Lcard22.setBounds(1220, 190, 70, 0);

        Lcard23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard23.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard23MousePressed(evt);
            }
        });
        getContentPane().add(Lcard23);
        Lcard23.setBounds(1220, 80, 70, 0);

        Lcard24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard24.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard24MousePressed(evt);
            }
        });
        getContentPane().add(Lcard24);
        Lcard24.setBounds(1140, 80, 70, 0);

        Lcard25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard25.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard25MousePressed(evt);
            }
        });
        getContentPane().add(Lcard25);
        Lcard25.setBounds(1060, 80, 70, 0);

        Lcard26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard26.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard26MousePressed(evt);
            }
        });
        getContentPane().add(Lcard26);
        Lcard26.setBounds(980, 80, 70, 0);

        Lcard27.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard27.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard27MousePressed(evt);
            }
        });
        getContentPane().add(Lcard27);
        Lcard27.setBounds(820, 80, 70, 0);

        Lcard28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard28.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard28MousePressed(evt);
            }
        });
        getContentPane().add(Lcard28);
        Lcard28.setBounds(900, 80, 70, 0);

        Lcard29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard29.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard29MousePressed(evt);
            }
        });
        getContentPane().add(Lcard29);
        Lcard29.setBounds(980, 190, 70, 0);

        Lcard30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard30.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard30MousePressed(evt);
            }
        });
        getContentPane().add(Lcard30);
        Lcard30.setBounds(1060, 190, 70, 0);

        Lcard31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard31.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard31MousePressed(evt);
            }
        });
        getContentPane().add(Lcard31);
        Lcard31.setBounds(1140, 190, 70, 0);

        Lcard32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard32.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard32MousePressed(evt);
            }
        });
        getContentPane().add(Lcard32);
        Lcard32.setBounds(1220, 300, 70, 0);

        Lcard33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard33.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard33MousePressed(evt);
            }
        });
        getContentPane().add(Lcard33);
        Lcard33.setBounds(1140, 300, 70, 0);

        Lcard34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard34.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard34MousePressed(evt);
            }
        });
        getContentPane().add(Lcard34);
        Lcard34.setBounds(1060, 300, 70, 0);

        Lcard35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard35.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard35MousePressed(evt);
            }
        });
        getContentPane().add(Lcard35);
        Lcard35.setBounds(980, 300, 70, 0);

        Lcard36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard36.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard36MousePressed(evt);
            }
        });
        getContentPane().add(Lcard36);
        Lcard36.setBounds(900, 300, 70, 0);

        Lcard37.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard37.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard37MousePressed(evt);
            }
        });
        getContentPane().add(Lcard37);
        Lcard37.setBounds(820, 300, 70, 0);

        Lcard38.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard38.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard38MousePressed(evt);
            }
        });
        getContentPane().add(Lcard38);
        Lcard38.setBounds(740, 300, 70, 0);

        Lcard39.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard39.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard39MousePressed(evt);
            }
        });
        getContentPane().add(Lcard39);
        Lcard39.setBounds(100, 300, 70, 0);

        Lcard40.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard40.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard40MousePressed(evt);
            }
        });
        getContentPane().add(Lcard40);
        Lcard40.setBounds(660, 80, 70, 0);

        Lcard41.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard41.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard41MousePressed(evt);
            }
        });
        getContentPane().add(Lcard41);
        Lcard41.setBounds(740, 80, 70, 0);

        Lcard42.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard42.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard42MousePressed(evt);
            }
        });
        getContentPane().add(Lcard42);
        Lcard42.setBounds(900, 190, 70, 0);

        Lcard43.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard43.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard43MousePressed(evt);
            }
        });
        getContentPane().add(Lcard43);
        Lcard43.setBounds(820, 190, 70, 0);

        Lcard44.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard44.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard44MousePressed(evt);
            }
        });
        getContentPane().add(Lcard44);
        Lcard44.setBounds(740, 190, 70, 0);

        Lcard45.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard45.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard45MousePressed(evt);
            }
        });
        getContentPane().add(Lcard45);
        Lcard45.setBounds(660, 190, 70, 0);

        Lcard46.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard46.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard46MousePressed(evt);
            }
        });
        getContentPane().add(Lcard46);
        Lcard46.setBounds(580, 190, 70, 0);

        Lcard47.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard47.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard47MousePressed(evt);
            }
        });
        getContentPane().add(Lcard47);
        Lcard47.setBounds(500, 190, 70, 0);

        Lcard48.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard48.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard48MousePressed(evt);
            }
        });
        getContentPane().add(Lcard48);
        Lcard48.setBounds(420, 190, 70, 0);

        Lcard49.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard49.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard49MousePressed(evt);
            }
        });
        getContentPane().add(Lcard49);
        Lcard49.setBounds(340, 190, 70, 0);

        Lcard50.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard50.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard50MousePressed(evt);
            }
        });
        getContentPane().add(Lcard50);
        Lcard50.setBounds(260, 190, 70, 0);

        Lcard51.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard51.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard51MousePressed(evt);
            }
        });
        getContentPane().add(Lcard51);
        Lcard51.setBounds(180, 190, 70, 0);

        Lcard52.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard52.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard52MousePressed(evt);
            }
        });
        getContentPane().add(Lcard52);
        Lcard52.setBounds(100, 190, 70, 0);

        Lcard53.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard53.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard53MousePressed(evt);
            }
        });
        getContentPane().add(Lcard53);
        Lcard53.setBounds(1220, 410, 70, 0);

        Lcard54.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard54.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard54MousePressed(evt);
            }
        });
        getContentPane().add(Lcard54);
        Lcard54.setBounds(1220, 520, 70, 0);

        Lcard55.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard55.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard55MousePressed(evt);
            }
        });
        getContentPane().add(Lcard55);
        Lcard55.setBounds(1140, 520, 70, 0);

        Lcard56.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard56.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard56MousePressed(evt);
            }
        });
        getContentPane().add(Lcard56);
        Lcard56.setBounds(1060, 520, 70, 0);

        Lcard57.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard57.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard57MousePressed(evt);
            }
        });
        getContentPane().add(Lcard57);
        Lcard57.setBounds(980, 520, 70, 0);

        Lcard58.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard58.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard58MousePressed(evt);
            }
        });
        getContentPane().add(Lcard58);
        Lcard58.setBounds(580, 80, 70, 0);

        Lcard59.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard59.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard59MousePressed(evt);
            }
        });
        getContentPane().add(Lcard59);
        Lcard59.setBounds(500, 80, 70, 0);

        Lcard60.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard60.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard60MousePressed(evt);
            }
        });
        getContentPane().add(Lcard60);
        Lcard60.setBounds(420, 80, 70, 0);

        Lcard61.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard61.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard61MousePressed(evt);
            }
        });
        getContentPane().add(Lcard61);
        Lcard61.setBounds(340, 80, 70, 0);

        Lcard62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard62.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard62MousePressed(evt);
            }
        });
        getContentPane().add(Lcard62);
        Lcard62.setBounds(180, 80, 70, 0);

        Lcard63.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard63.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard63MousePressed(evt);
            }
        });
        getContentPane().add(Lcard63);
        Lcard63.setBounds(100, 80, 70, 0);

        Lcard64.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cartasDificil/Tapada.png"))); // NOI18N
        Lcard64.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                Lcard64MousePressed(evt);
            }
        });
        getContentPane().add(Lcard64);
        Lcard64.setBounds(260, 80, 70, 0);

        Lwin.setFont(new java.awt.Font("04b", 0, 24)); // NOI18N
        Lwin.setForeground(new java.awt.Color(0, 0, 0));
        getContentPane().add(Lwin);
        Lwin.setBounds(510, 20, 870, 40);

        Lintentos.setFont(new java.awt.Font("04b", 0, 36)); // NOI18N
        Lintentos.setForeground(new java.awt.Color(0, 0, 0));
        getContentPane().add(Lintentos);
        Lintentos.setBounds(420, 10, 260, 50);

        Lfondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/FondoDificil2.jpg"))); // NOI18N
        getContentPane().add(Lfondo);
        Lfondo.setBounds(0, -60, 1640, 850);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Lcard63MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard63MousePressed
        destaparCarta(Lcard63);
    }//GEN-LAST:event_Lcard63MousePressed

    private void Lcard62MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard62MousePressed
        destaparCarta(Lcard62);
    }//GEN-LAST:event_Lcard62MousePressed

    private void Lcard64MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard64MousePressed
      destaparCarta(Lcard64);
    }//GEN-LAST:event_Lcard64MousePressed

    private void Lcard61MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard61MousePressed
       destaparCarta(Lcard61);
    }//GEN-LAST:event_Lcard61MousePressed

    private void Lcard60MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard60MousePressed
       destaparCarta(Lcard60);
    }//GEN-LAST:event_Lcard60MousePressed

    private void Lcard59MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard59MousePressed
     destaparCarta(Lcard59);
    }//GEN-LAST:event_Lcard59MousePressed

    private void Lcard58MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard58MousePressed
       destaparCarta(Lcard58);
    }//GEN-LAST:event_Lcard58MousePressed

    private void Lcard40MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard40MousePressed
       destaparCarta(Lcard40);
    }//GEN-LAST:event_Lcard40MousePressed

    private void Lcard41MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard41MousePressed
        destaparCarta(Lcard41);
    }//GEN-LAST:event_Lcard41MousePressed

    private void Lcard27MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard27MousePressed
      destaparCarta(Lcard27);
    }//GEN-LAST:event_Lcard27MousePressed

    private void Lcard28MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard28MousePressed
      destaparCarta(Lcard28);
    }//GEN-LAST:event_Lcard28MousePressed

    private void Lcard26MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard26MousePressed
        destaparCarta(Lcard26);
    }//GEN-LAST:event_Lcard26MousePressed

    private void Lcard25MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard25MousePressed
        destaparCarta(Lcard25);
    }//GEN-LAST:event_Lcard25MousePressed

    private void Lcard24MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard24MousePressed
       destaparCarta(Lcard24);
    }//GEN-LAST:event_Lcard24MousePressed

    private void Lcard23MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard23MousePressed
       destaparCarta(Lcard23);
    }//GEN-LAST:event_Lcard23MousePressed

    private void Lcard52MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard52MousePressed
        destaparCarta(Lcard52);
    }//GEN-LAST:event_Lcard52MousePressed

    private void Lcard51MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard51MousePressed
        destaparCarta(Lcard51);
    }//GEN-LAST:event_Lcard51MousePressed

    private void Lcard50MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard50MousePressed
       destaparCarta(Lcard50);
    }//GEN-LAST:event_Lcard50MousePressed

    private void Lcard49MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard49MousePressed
        destaparCarta(Lcard49);
    }//GEN-LAST:event_Lcard49MousePressed

    private void Lcard48MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard48MousePressed
        destaparCarta(Lcard48);
    }//GEN-LAST:event_Lcard48MousePressed

    private void Lcard47MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard47MousePressed
       destaparCarta(Lcard47);
    }//GEN-LAST:event_Lcard47MousePressed

    private void Lcard46MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard46MousePressed
      destaparCarta(Lcard46);
    }//GEN-LAST:event_Lcard46MousePressed

    private void Lcard45MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard45MousePressed
       destaparCarta(Lcard45);
    }//GEN-LAST:event_Lcard45MousePressed

    private void Lcard44MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard44MousePressed
       destaparCarta(Lcard44);
    }//GEN-LAST:event_Lcard44MousePressed

    private void Lcard43MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard43MousePressed
     destaparCarta(Lcard43);
    }//GEN-LAST:event_Lcard43MousePressed

    private void Lcard42MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard42MousePressed
       destaparCarta(Lcard42);
    }//GEN-LAST:event_Lcard42MousePressed

    private void Lcard29MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard29MousePressed
       destaparCarta(Lcard29);
    }//GEN-LAST:event_Lcard29MousePressed

    private void Lcard30MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard30MousePressed
      destaparCarta(Lcard30);
    }//GEN-LAST:event_Lcard30MousePressed

    private void Lcard31MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard31MousePressed
        destaparCarta(Lcard31);
    }//GEN-LAST:event_Lcard31MousePressed

    private void Lcard39MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard39MousePressed
       destaparCarta(Lcard39);
    }//GEN-LAST:event_Lcard39MousePressed

    private void Lcard15MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard15MousePressed
       destaparCarta(Lcard15);
    }//GEN-LAST:event_Lcard15MousePressed

    private void Lcard16MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard16MousePressed
       destaparCarta(Lcard16);
    }//GEN-LAST:event_Lcard16MousePressed

    private void Lcard17MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard17MousePressed
        destaparCarta(Lcard17);
    }//GEN-LAST:event_Lcard17MousePressed

    private void Lcard18MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard18MousePressed
        destaparCarta(Lcard18);
    }//GEN-LAST:event_Lcard18MousePressed

    private void Lcard19MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard19MousePressed
      destaparCarta(Lcard19);
    }//GEN-LAST:event_Lcard19MousePressed

    private void Lcard20MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard20MousePressed
       destaparCarta(Lcard20);
    }//GEN-LAST:event_Lcard20MousePressed

    private void Lcard21MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard21MousePressed
      destaparCarta(Lcard21);
    }//GEN-LAST:event_Lcard21MousePressed

    private void Lcard38MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard38MousePressed
      destaparCarta(Lcard38);
    }//GEN-LAST:event_Lcard38MousePressed

    private void Lcard37MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard37MousePressed
       destaparCarta(Lcard37);
    }//GEN-LAST:event_Lcard37MousePressed

    private void Lcard36MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard36MousePressed
        destaparCarta(Lcard36);
    }//GEN-LAST:event_Lcard36MousePressed

    private void Lcard35MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard35MousePressed
        destaparCarta(Lcard35);
    }//GEN-LAST:event_Lcard35MousePressed

    private void Lcard34MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard34MousePressed
       destaparCarta(Lcard34);
    }//GEN-LAST:event_Lcard34MousePressed

    private void Lcard33MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard33MousePressed
      destaparCarta(Lcard33);
    }//GEN-LAST:event_Lcard33MousePressed

    private void Lcard32MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard32MousePressed
        destaparCarta(Lcard32);
    }//GEN-LAST:event_Lcard32MousePressed

    private void Lcard14MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard14MousePressed
        destaparCarta(Lcard14);
    }//GEN-LAST:event_Lcard14MousePressed

    private void Lcard5MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard5MousePressed
       destaparCarta(Lcard5);
    }//GEN-LAST:event_Lcard5MousePressed

    private void Lcard3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard3MousePressed
       destaparCarta(Lcard3);
    }//GEN-LAST:event_Lcard3MousePressed

    private void Lcard4MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard4MousePressed
         destaparCarta(Lcard4);
    }//GEN-LAST:event_Lcard4MousePressed

    private void Lcard6MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard6MousePressed
        destaparCarta(Lcard6);
    }//GEN-LAST:event_Lcard6MousePressed

    private void Lcard7MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard7MousePressed
         destaparCarta(Lcard7);
    }//GEN-LAST:event_Lcard7MousePressed

    private void Lcard2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard2MousePressed
       destaparCarta(Lcard2);
    }//GEN-LAST:event_Lcard2MousePressed

    private void Lcard1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard1MousePressed
         destaparCarta(Lcard1);
    }//GEN-LAST:event_Lcard1MousePressed

    private void Lcard8MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard8MousePressed
        destaparCarta(Lcard8);
    }//GEN-LAST:event_Lcard8MousePressed

    private void Lcard9MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard9MousePressed
         destaparCarta(Lcard9);
    }//GEN-LAST:event_Lcard9MousePressed

    private void Lcard10MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard10MousePressed
       destaparCarta(Lcard10);
    }//GEN-LAST:event_Lcard10MousePressed

    private void Lcard11MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard11MousePressed
         destaparCarta(Lcard11);
    }//GEN-LAST:event_Lcard11MousePressed

    private void Lcard12MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard12MousePressed
        destaparCarta(Lcard12);
    }//GEN-LAST:event_Lcard12MousePressed

    private void Lcard13MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard13MousePressed
        destaparCarta(Lcard13);
    }//GEN-LAST:event_Lcard13MousePressed

    private void Lcard53MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard53MousePressed
        destaparCarta(Lcard53);
    }//GEN-LAST:event_Lcard53MousePressed

    private void Lcard57MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard57MousePressed
         destaparCarta(Lcard57);
    }//GEN-LAST:event_Lcard57MousePressed

    private void Lcard56MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard56MousePressed
         destaparCarta(Lcard56);
    }//GEN-LAST:event_Lcard56MousePressed

    private void Lcard55MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard55MousePressed
         destaparCarta(Lcard55);
    }//GEN-LAST:event_Lcard55MousePressed

    private void Lcard54MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard54MousePressed
        destaparCarta(Lcard54);
    }//GEN-LAST:event_Lcard54MousePressed

    private void Lcard22MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Lcard22MousePressed
        destaparCarta(Lcard22);
    }//GEN-LAST:event_Lcard22MousePressed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
          if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            if(win>=32){
                this.dispose();
                FrmInicio fi = new FrmInicio();
                fi.setVisible(true);
            }
        }
        if(evt.getKeyCode() == KeyEvent.VK_BACK_SPACE){
             this.dispose();
                FrmInicio fi = new FrmInicio();
                fi.setVisible(true);
        }
    }//GEN-LAST:event_formKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmDificil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmDificil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmDificil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmDificil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmDificil().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Lacierto_error;
    private javax.swing.JLabel Lcard1;
    private javax.swing.JLabel Lcard10;
    private javax.swing.JLabel Lcard11;
    private javax.swing.JLabel Lcard12;
    private javax.swing.JLabel Lcard13;
    private javax.swing.JLabel Lcard14;
    private javax.swing.JLabel Lcard15;
    private javax.swing.JLabel Lcard16;
    private javax.swing.JLabel Lcard17;
    private javax.swing.JLabel Lcard18;
    private javax.swing.JLabel Lcard19;
    private javax.swing.JLabel Lcard2;
    private javax.swing.JLabel Lcard20;
    private javax.swing.JLabel Lcard21;
    private javax.swing.JLabel Lcard22;
    private javax.swing.JLabel Lcard23;
    private javax.swing.JLabel Lcard24;
    private javax.swing.JLabel Lcard25;
    private javax.swing.JLabel Lcard26;
    private javax.swing.JLabel Lcard27;
    private javax.swing.JLabel Lcard28;
    private javax.swing.JLabel Lcard29;
    private javax.swing.JLabel Lcard3;
    private javax.swing.JLabel Lcard30;
    private javax.swing.JLabel Lcard31;
    private javax.swing.JLabel Lcard32;
    private javax.swing.JLabel Lcard33;
    private javax.swing.JLabel Lcard34;
    private javax.swing.JLabel Lcard35;
    private javax.swing.JLabel Lcard36;
    private javax.swing.JLabel Lcard37;
    private javax.swing.JLabel Lcard38;
    private javax.swing.JLabel Lcard39;
    private javax.swing.JLabel Lcard4;
    private javax.swing.JLabel Lcard40;
    private javax.swing.JLabel Lcard41;
    private javax.swing.JLabel Lcard42;
    private javax.swing.JLabel Lcard43;
    private javax.swing.JLabel Lcard44;
    private javax.swing.JLabel Lcard45;
    private javax.swing.JLabel Lcard46;
    private javax.swing.JLabel Lcard47;
    private javax.swing.JLabel Lcard48;
    private javax.swing.JLabel Lcard49;
    private javax.swing.JLabel Lcard5;
    private javax.swing.JLabel Lcard50;
    private javax.swing.JLabel Lcard51;
    private javax.swing.JLabel Lcard52;
    private javax.swing.JLabel Lcard53;
    private javax.swing.JLabel Lcard54;
    private javax.swing.JLabel Lcard55;
    private javax.swing.JLabel Lcard56;
    private javax.swing.JLabel Lcard57;
    private javax.swing.JLabel Lcard58;
    private javax.swing.JLabel Lcard59;
    private javax.swing.JLabel Lcard6;
    private javax.swing.JLabel Lcard60;
    private javax.swing.JLabel Lcard61;
    private javax.swing.JLabel Lcard62;
    private javax.swing.JLabel Lcard63;
    private javax.swing.JLabel Lcard64;
    private javax.swing.JLabel Lcard7;
    private javax.swing.JLabel Lcard8;
    private javax.swing.JLabel Lcard9;
    private javax.swing.JLabel Lfondo;
    private javax.swing.JLabel Lintentos;
    private javax.swing.JLabel Lwin;
    // End of variables declaration//GEN-END:variables
}
